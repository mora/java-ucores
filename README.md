======
README
======

Java for UCores – UCores and accelerator 'friendly' Java version based on OpenJDK[1] and obuildfactory[2]

=====
Notes
=====

This is an attempt at building a custom Java version that will address the special needs of unconventional cores (UCores) and accelerators.
The current emphasis is on improving java’s memory allocation strategy for UCores and accelerators.
Release/test versions can be found at the [dist folder](dist/README.md)
For questions please contact oren_segal at uml dot edu

==========
References
==========
[1] http://openjdk.java.net/ 
[2] https://github.com/hgomez/obuildfactory




