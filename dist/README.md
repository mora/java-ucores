===================
Distribution folder
===================

-----
Notes
-----

Because of file sizes only links to compiled JDK/JRE versions go in this folder

--------------------------
Initial beta release v0.0.1b
--------------------------
In this preliminary experimental version we are enabling java programmers/users to allocate aligned memory in Java automatically. 
The motivation behind this experimental feature is that on one hand some accelerators rely on fast memory transfers using PCIE etc. and/or have special memory alignment requirements to maximize performance and on the other hand we want to avoid having to modify user code and/or burdening the user with intimate knowledge of the underlying hardware.
This version automatically allocates aligned memory without annotation or user intervention under the following conditions:
1.	When using arrays of primitive types in Java (byte/char/int/long/float/double etc.)
2.	When array size is bigger or equal 4Kbytes

Memory is currently aligned on 64 byte boundaries since it works well in our initial tests on various internal systems and with our family of FPGA accelerators. 
Next versions are planned to be flexible and allow customization of alignment, min array sizes, data types etc.

-----------------------
Usage instructions
------------------------
1.	Download the provided zipped Java Run Time Environment (JRE)
2.	Extract to a folder on the local file system
3.	Set PATH to new JRE location

---------------------------------
Download links below
---------------------------------

---------------------------------------------------------------------------------------------
Build version	|	build type			|	download link
---------------------------------------------------------------------------------------------
v0.1.0b		| openjdk7, linux-amd64, j2re-image	| j2re-image-ucores-201603061711.zip [https://goo.gl/n0p4GS]
---------------------------------------------------------------------------------------------


